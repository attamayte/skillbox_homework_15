#include <iostream>

// Could be Enum, but for such a tiny app it doesn't matter

//static enum { PRINT_EVEN = 0, PRINT_ODD = 1 };
static const bool PRINT_EVEN{ 0 }, PRINT_ODD{ 1 };

// both are <static>, though should be seen only in main cpp


// ----------------- HOMEWORK FUNCTION --------------------
// Prints even or odd numbers to std::cout in range 0 --> N
// First param set the range; expected values 0 --> INT_MAX 
// Second param determines what values to print: odd | even
// True will print odd numbers, False -- print even numbers
void PrintZeroToN(const int N, const bool PRINT_ODD = true)
{
    for (int i = PRINT_ODD; i <= N; i += 2)
        std::cout << i << '\n';
}


int main()
{
    PrintZeroToN(16, PRINT_EVEN);
    PrintZeroToN(33, PRINT_ODD);
    PrintZeroToN(5 + 4, 5 - 4);
    
    // would not work for int counter: N is less than 0 & 1
    // unsigned int counter will throw the cycle to bananas
    PrintZeroToN(-4663, PRINT_EVEN); 

    return 0;
}
